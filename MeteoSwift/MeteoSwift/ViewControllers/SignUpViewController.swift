import UIKit
import FirebaseAuth

class SignUpViewController: UIViewController {

    @IBOutlet weak var EmailTextField: UITextField!
    @IBOutlet weak var PasswordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()

    }
    
    func initView()
    {
        EmailTextField.placeholder = "E-mail"
        PasswordTextField.placeholder = "Password"
    }
    
    @IBAction func SignUpAction(_ sender: Any) {
        Auth.auth().createUser(withEmail: EmailTextField.text!, password: PasswordTextField.text!) { authResult, error in
            if error != nil
                {
                    let alert = UIAlertController(title: "Oops...", message: "The Email or password is incorrect", preferredStyle: UIAlertController.Style.alert)

                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                    
                    self.present(alert, animated: true, completion: nil)
                }
            else
            {
                self.performSegue(withIdentifier: "SignUpSucessSegueID", sender: nil)
            }


        }
    }
    
    @IBAction func LogInAction(_ sender: Any) {
        dismiss(animated: true)
    }
}
