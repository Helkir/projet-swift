import Foundation
import Alamofire
import SwiftyJSON

public class NetworkManager {
    
    public  static func getRequest(path : String , param : [String:Any], listener : NetworkProtocol){
        let url = NetworkConst.base_url + path
        
        
        Alamofire.request(url, method: HTTPMethod.get, parameters: param, encoding: URLEncoding.default,headers : nil).responseJSON
            {
                response in
                
                if let json = response.result.value {
                    listener.handleJSON(json: JSON(json))
                    
                    
                }
                
                if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                    print("Data: \(utf8Text)")
                }
        }
        
    }
    
    public  static func postRequest(path : String , param : [String : Any],listner : NetworkProtocol){
        let url = NetworkConst.base_url + path
        Alamofire.request(url, method: .post, parameters: param, encoding: URLEncoding.default, headers: nil).responseJSON
            {
                response in
                
                
                if let json = response.result.value {
                    
                    listner.handleJSON(json: JSON(json))
                    
                }
                
                if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                    print("Data: \(utf8Text)")
                }
        }
        
    }
}
