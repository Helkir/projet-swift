//
//  NetworkConst.swift
//  MeteoSwift
//
//  Created by Arnaud on 08/01/2020.
//  Copyright © 2020 Arnaud. All rights reserved.
//

import Foundation
public class NetworkConst {

    public static var base_url = "https://api.openweathermap.org/data/2.5/"
    public static var path = "weather"
    public static var apiKey = "14e64438c73fd7f3e0644c912c2a39e1"
}
