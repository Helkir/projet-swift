//
//  Meteo.swift
//  MeteoSwift
//
//  Created by Arnaud on 08/01/2020.
//  Copyright © 2020 Arnaud. All rights reserved.
//

import Foundation
import SwiftyJSON

public class Meteo {
    public var weather: String?
    public var temperature: Float?
    public var humidity: Float?
    public var vent: Float?
    
    public init(json: JSON) {
        self.weather = json["weather"][0]["main"].stringValue
        self.temperature = json["main"]["temp"].floatValue
        self.humidity = json["main"]["humidity"].floatValue
        self.vent = json["wind"]["speed"].floatValue
        
    }
}
