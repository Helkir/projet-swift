
import UIKit
import FirebaseAuth

class HomeViewController: UIViewController, WeatherProtocol {

    @IBOutlet weak var LabelAccueil: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var meteo = Helper.sharedInstance.meteo
    var presenter: WeatherPresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let user = Auth.auth().currentUser?.email
        LabelAccueil.text = user
        initView()
    }
    
    func initView() {
        presenter = WeatherPresenter(listener: self)
        navigationItem.title = "Meteo"
        navigationController?.navigationBar.prefersLargeTitles = false
        presenter?.getWeather(city: "Paris")
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func handleWeather(api: [Meteo]) {
        tableView.reloadData()
    }
    
    @IBAction func LogoutAction(_ sender: Any) {
        UserDefaults.standard.removeObject(forKey: "userissignedin")
        self.performSegue(withIdentifier: "LogoutSegueID", sender: nil)
    }
    
    @IBAction func comparerAction(_ sender: Any) {
 
    /*  DispatchQueue.main.asyncAfter(deadline: .now() + 6) {
          self.activityIndicator.isHidden = true
          self.activityIndicator.stopAnimating()
      } */
      
    }
    
        
}
    
extension HomeViewController: UITableViewDelegate {}
extension HomeViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return meteo.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WeatherCellID", for: indexPath) as! WeatherTableViewCell
        
        cell.weatherLabel.text = meteo[indexPath.item].weather
        
        return cell
    }
    
    
}

