import UIKit
import FirebaseAuth
import AudioToolbox

class LoginViewController: UIViewController {
    
    //Outlets
    @IBOutlet weak var EmailTextField: UITextField!
    @IBOutlet weak var PasswordTextField: UITextField!
    @IBOutlet weak var MeteoImg: UIImageView!
    
    let userDefault = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        
    }
    
    func initView()
    {
        EmailTextField.delegate = self
        PasswordTextField.delegate = self
        EmailTextField.placeholder = "E-mail"
        PasswordTextField.placeholder = "Password"
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        MeteoImg.isUserInteractionEnabled = true
        MeteoImg.addGestureRecognizer(tapGestureRecognizer)
        
    }
    //Actions
    
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let tappedImage = tapGestureRecognizer.view as! UIImageView
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        print("vibration")
    }
    
    
    @IBAction func LogInAction(_ sender: Any) {
        Auth.auth().signIn(withEmail: EmailTextField.text!, password: PasswordTextField.text!) { [weak self] authResult, error in
            
            if error != nil {
                print(error)
                let alert = UIAlertController(title: "Oops...", message: "The Email or password is incorrect", preferredStyle: UIAlertController.Style.alert)
                
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                
                self!.present(alert, animated: true, completion: nil)
            }else{
                self!.userDefault.set(true,forKey: "userissignedin")
                self?.performSegue(withIdentifier: "LoginSuccessSegueID", sender: nil)
            }
        }
    }
    
    @IBAction func SignUpAction(_ sender: Any) {
        performSegue(withIdentifier: "SignUpSegueID", sender: nil)
    }
    
    
}

extension LoginViewController: UITextFieldDelegate{}
