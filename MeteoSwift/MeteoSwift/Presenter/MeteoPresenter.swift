import Foundation
import SwiftyJSON
import Alamofire

public protocol NetworkProtocol {
    func handleJSON(json: JSON)
}

public protocol WeatherProtocol {
    func handleWeather(api: [Meteo])
}

public class WeatherPresenter : NetworkProtocol {
    
    var listener : WeatherProtocol?
    
    public init(listener : WeatherProtocol) {
        self.listener = listener
    }
    
    public func handleJSON(json: JSON) {
        let jsonObject = json
        for i in 0..<jsonObject.count {
            let x = Meteo(json: jsonObject[i])
            Helper.sharedInstance.meteo.append(x)
        }
        listener?.handleWeather(api: Helper.sharedInstance.meteo)
    }
    
    public func getWeather(city: String) {
        let param : [String:Any] = [
            "q": city,
            "apiKey": NetworkConst.apiKey]
        NetworkManager.getRequest(path: NetworkConst.path, param: param, listener: self)
        
    }
}
