import UIKit
import FirebaseAuth

class LaunchScreenViewController: UIViewController {

    
    let userDefaults = UserDefaults.standard
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    override func viewDidAppear(_ animated: Bool) {
        if userDefaults.bool(forKey: "userissignedin") == true
        {
            performSegue(withIdentifier: "UserIsSignedInSequeID", sender: nil)
        }
        else
        {
            performSegue(withIdentifier: "LoginSegueID", sender: nil)
    
        }
        
    }
}
